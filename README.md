# RieszRadiomics #

Source code for predicting lung adenocarcinoma survival using the Generalized Riesz-Wavelet Toolbox for Matlab http://bigwww.epfl.ch/demo/steerable-wavelets/ and the R Glmnet package for variable selection in the Cox models https://web.stanford.edu/~hastie/glmnet/glmnet_alpha.html.
 
The code was developped during a research internship at the Stanford Laboratory of Quantitative Imaging Stanford https://rubinlab.stanford.edu/, under the supervision of :

- Pr. Daniel Rubin https://profiles.stanford.edu/daniel-rubin
- Pr. Adrien Depeursinge http://medgift.hevs.ch/wordpress/team/adrien-depeursinge/
- Dr. Todd Aguilera http://profiles.utsouthwestern.edu/profile/176733/todd-aguilera.html�

A special thanks to Assaf Hoogi https://profiles.stanford.edu/assaf-hoogi, the principal investigator of the lab during the internship.

## Code content ##

The matlab/ directory contains mainMATLAB.m file, that :

* extracts ROI and CT slices from dicom files into matlab files
* selects the optimal slice and ROI to use
* computes and saves Riesz wavelet energies on selected ROIs
* extracts and saves survival information

The r/ directory contains coxLasso_fullNodule.R and coxLasso_innerOuter.R files that :

* use the Glmnet package to select Cox models through cross-validation in the train set 
* separate patient populations according to predicted survivals in the test set
* compute and save Log-Rank p-values by comparing separated test set populations

The data/ directory contains all data used during computation :

* csv/ contains features and labels used by the R survival analysis code
* dicom/ contains dicom raw images
* img/ contains Kaplan-Meyer and Log-Rank p-value illustrations
* mat/, matIMG/ and matSET/ contain post-processed dicom images saved as .mat files

## Patient information ##

Patient information has been removed from this public repository. Here is where it used to be stored :

* data/dicom/ should contain dicom patient images
* matlab/dicom/createDictionnary.m should contain a dictionnary which, for each patient, gives the name of the ROI to use
* ./ should contain an Excel file with patient survival information.