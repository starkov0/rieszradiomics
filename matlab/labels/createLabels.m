% import recurence and survival data
% combine :
% 1. local recurrence event
% 2. local recurrence time
% 3. regional recurrence event
% 4. regional recurrence time
% 5. distal recurrence event
% 6. distal recurrence time
% 7. overall survival event
% 8. overall survival time
% and save it as csv

importRecurrence;
importSurvival;

caseROIIndex = load('../data/mat/caseROIIndex.mat');
caseROIIndex = caseROIIndex.caseROIIndex;

labels = [LC(caseROIIndex(:,1)),...
    LCTime(caseROIIndex(:,1)),...
    RC(caseROIIndex(:,1)),...
    RCTime(caseROIIndex(:,1)),...
    DM(caseROIIndex(:,1)),...
    DMTime(caseROIIndex(:,1)),...
    OS(caseROIIndex(:,1)),...
    OSTime(caseROIIndex(:,1))];

% add an extra line at the beginning of the file so R can read it correctly
labels = [linspace(1,size(labels,2),size(labels,2)); labels];

csvwrite('../data/csv/labels.csv',labels);