% Create data permutations that will later be used in cross-validation.
% Due to limitations in the number of positive events, permutations are
% performed on positive and negative events separately.

status = csvread('../data/csv/labels.csv');

index = {};
index{1} = find(status(:,1) > 0);
index{2} = find((status(:,1)+status(:,3)) > 0);
index{3} = find((status(:,1)+status(:,3)+status(:,5)) > 0);
index{4} = find(status(:,5) > 0);
index{5} = find((status(:,1)+status(:,3)+status(:,5)+status(:,7)) > 0);
index{6} = find(status(:,7) > 0);

for j=1:6
    
    array0 = [];
    array1 = [];
    
    for i=1:1000
        index1 = index{j};
        index0 = setdiff(1:116,index1)';

        randsample0 = randsample(length(index0),length(index0));
        randsample1 = randsample(length(index1),length(index1));

        array0 = [array0, index0(randsample0)];
        array1 = [array1, index1(randsample1)];

    end

    csvwrite(['../data/csv/permutationsArray',num2str(j),num2str(0),'.csv'],array0);
    csvwrite(['../data/csv/permutationsArray',num2str(j),num2str(1),'.csv'],array1);
    
end
