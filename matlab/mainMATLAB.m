% The following code is intended to test survival predictions based on
% region of interests in medical imaging. The code has been used on a
% lung adenocarcinoma CT database that was accompanied with survival and
% metastasis information.

% add all subdirectories in the matlab path
addpath(genpath(pwd));

% file found in dicom/
dicom2MatSet;

% file found in features/
createFeatures;

% file found in labels/
createLabels;

% files found in permutaitions/
createPermutations;