% open matlab slice + ROI mask
% apply a gaussian filter on the slice
% extract riesz energies from the slice + ROI
% save features in a CSV file, to be used in R later on

matIMGPath = '../data/matIMG/';
matIMGFiles = dir([matIMGPath,'*.mat']);


for sigma = [0,3]
    for n=1:5
        for j=1:5
            
            imagingFeatures = [];
            for i=1:length(matIMGFiles)

                display([num2str(n),'-',num2str(j),'-',num2str(i)]);

                raw = load([matIMGPath,matIMGFiles(i).name]);    
                image = raw.slice;
                mask = raw.mask;

                if sigma
                    H = fspecial('gaussian', [3 3], sigma);
                    image = imfilter(image,H,'replicate');
                end

                features = lesionRiesz(image, mask, n, j);
                imagingFeatures = [imagingFeatures; features];

                if i==1
                    imagingFeatures = [imagingFeatures; features];
                end

            end
            
            gaussText = 'noGaussian';
            if sigma > 0
                gaussText = 'Gaussian';
            end
            
            csvwrite(['../data/csv/',gaussText,'/imagingFeatures',num2str(n),'_',num2str(j),'.csv'],imagingFeatures);
            
        end
    end
end
