%% Import data from spreadsheet
% Script for importing data from the following spreadsheet:
%
%    Workbook: /Users/pierrestarkov/stanford/lung2/todd/9-18-15 SABR stg 1
%    3D Img +scans.xlsx Worksheet: Sheet1
%
% To extend the code for use with different selected data or a different
% spreadsheet, generate a function instead of a script.

% Auto-generated by MATLAB on 2015/11/04 00:08:23

%% Import the data
[~, ~, HistologyImport] = xlsread('/Users/pierrestarkov/stanford/lung2/todd/9-18-15 SABR stg 1 3D Img +scans.xlsx','Sheet1');
HistologyImport = HistologyImport(:,48);
HistologyImport(cellfun(@(x) ~isempty(x) && isnumeric(x) && isnan(x),HistologyImport)) = {''};

HistologyImport = HistologyImport(2:end);

HistologyA = {'Adeno','Adenocarcinoma','Adenosquamous Carcinoma'};
HistologyS = {'SCC','Squamous','Squamous Cell Carcinoma'};

h1 = unique(HistologyImport);
HISTOLOGY = [];
for i=1:length(HistologyImport)
    
    HISTOLOGY(i) = 0;
    
    for j=1:length(HistologyA)
        if strcmp(HistologyA{j},HistologyImport{i})
            HISTOLOGY(i) = 1;
            break;
        end
    end
    
    for j=1:length(HistologyS)
        if strcmp(HistologyS{j},HistologyImport{i})
            HISTOLOGY(i) = 2;
            break;
        end
    end        
    
end