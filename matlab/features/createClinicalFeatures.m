importAge;
importBED;
importGender;
importHistology;
importLocation;
importStage;

caseROIIndex = load('../data/mat/caseROIIndex.mat');
caseROIIndex = caseROIIndex.caseROIIndex;

clinicalFeatures = [...
    AGE(caseROIIndex(:,1)),...
    BED(caseROIIndex(:,1)),...
    GENDER(caseROIIndex(:,1))',...
    HISTOLOGY(caseROIIndex(:,1))',...
    LOCATIONLR(caseROIIndex(:,1))',...
    LOCATIONUML(caseROIIndex(:,1))',...
    STAGE(caseROIIndex(:,1))'];

% add an extra line at the beginning of the file so R can read it correctly
clinicalFeatures = [linspace(1,size(clinicalFeatures,2),size(clinicalFeatures,2)); clinicalFeatures];

csvwrite('../data/csv/clinicalFeatures.csv',clinicalFeatures);