% open matlab slice + ROI mask
% compute tumor area feature by counting the number of pixels in the ROI mask
% save the feature in a CSV file, to be used in R later on


matSETPath = '../data/matSET/';
matSETFiles = dir([matSETPath,'*.mat']);

tumorArea = [];

for i=1:length(matSETFiles)
    
    display(num2str(i));
    
    raw = load([matSETPath,matSETFiles(i).name]);  
    masks = raw.masks;
    
    area = 0;
    for j=1:length(masks)
        area = area + sum(sum(raw.masks{j}));
    end
    
    tumorArea = [tumorArea; [i area]];
    
end

% add an extra line at the beginning of the file so R can read it correctly
tumorArea = [linspace(1,size(tumorArea,2),size(tumorArea,2)); tumorArea];

csvwrite('../data/csv/tumorArea.csv',tumorArea);
