% open matlab slice + ROI mask
% approximately separate ground-glass-opacity from the rest of the nodule
% by applying a morphological structuring element on the ROI
% apply a gaussian filter on both ROIs
% extract riesz energies from both ROIs
% save features in a CSV file, to be used in R later on

matIMGPath = '../data/matIMG/';
matIMGFiles = dir([matIMGPath,'*.mat']);

for sigma = [0,3]
    for n=1:5
        for j=1:5
            imagingFeaturesIn = [];
            imagingFeaturesOut = [];
            for i=1:length(matIMGFiles)
                display([num2str(n),'-',num2str(j),'-',num2str(i)]);

                raw = load([matIMGPath,matIMGFiles(i).name]);    
                image = raw.slice;
                mask = raw.mask;

                se = strel('diamond',3);
                mask1 = mask-imerode(mask,se);  % outer
                mask2 = mask-mask1;             % inner

                if sigma
                    H = fspecial('gaussian', [3 3], sigma);
                    image = imfilter(image,H,'replicate');
                end

                featuresIn = lesionRiesz(image, mask2, n, j);
                featuresOut = lesionRiesz(image, mask1, n, j);           

                imagingFeaturesIn = [imagingFeaturesIn; featuresIn];
                imagingFeaturesOut = [imagingFeaturesOut; featuresOut];
                
                if i==1
                    imagingFeaturesIn = [imagingFeaturesIn; features];
                    imagingFeaturesOut = [imagingFeaturesOut; features];
                end
            end
            
            gaussText = 'noGaussian';
            if sigma > 0
                gaussText = 'Gaussian';
            end
            
            csvwrite(['../data/csv/',gaussText,'/imagingFeaturesIn',num2str(n),'_',num2str(j),'.csv'],imagingFeaturesIn);
            csvwrite(['../data/csv/',gaussText,'/imagingFeaturesOut',num2str(n),'_',num2str(j),'.csv'],imagingFeaturesOut);

        end
    end
end
