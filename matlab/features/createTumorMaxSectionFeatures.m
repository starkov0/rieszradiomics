% open matlab slice + ROI mask
% compute tumor maximal section feature through the use of ROI mask
% rotations.
% save the feature in a CSV file, to be used in R later on

matSETPath = '../data/matSET/';

matIMGPath = '../data/matIMG/';
matIMGFiles = dir([matIMGPath,'*.mat']);

tumorMaxSection = [];

for i=1:length(matIMGFiles)
    
    display(num2str(i));
    
    raw = load([matIMGPath,matIMGFiles(i).name]);    
    mask = raw.mask;
    
    maxSection = 0;
    
    [x,y] = find(mask);
    linesInMask = unique(y);
    angles = 1:10:360;
    
    for j=1:length(linesInMask)
        for k=angles
            rotatedMask = imrotate(mask,k);
            if maxSection < sum(rotatedMask(:,linesInMask(j)))
                maxSection = sum(rotatedMask(:,linesInMask(j)));
            end
        end
    end
    
    tumorMaxSection = [tumorMaxSection maxSection];
    
end

% add an extra line at the beginning of the file so R can read it correctly
tumorMaxSection = [linspace(1,size(tumorMaxSection,2),size(tumorMaxSection,2)); tumorMaxSection];

csvwrite('../data/csv/tumorMaxSection.csv',tumorMaxSection);
