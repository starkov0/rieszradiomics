% save the ROI (mask) and images per patient

display('createDcm2Data');

caseROIIndex = load('../data/mat/caseROIIndex.mat');
caseROIIndex = caseROIIndex.caseROIIndex;

dicomPath = '../data/dicom/';
dicomDirNames = dir([dicomPath,'*00']);
matSETPath = '../data/matSET/';

for i = 1:size(caseROIIndex,1)
    
    display(num2str(i));
        
    % dicom - data path
    dicomDirPath = [dicomPath,dicomDirNames(caseROIIndex(i,1)).name,'/'];
    dicomDirNameSplit = strsplit(dicomDirNames(caseROIIndex(i,1)).name,'-');
    dataFilePath = [matSETPath,dicomDirNameSplit{1},'.mat'];

    % info file path
    infoFileStruct = dir([dicomDirPath,'RS.*']);
    infoFilePath = infoFileStruct.name;
    
    % get slice names
    infoFile = dicominfo([dicomDirPath,infoFilePath]);
    itemNames = fieldnames(infoFile.StructureSetROISequence);
    contourNames = fieldnames(infoFile.ROIContourSequence.(itemNames{caseROIIndex(i,2)}).ContourSequence);

    % save slices and masks
    slices = {};
    masks = {};
    for j=1:length(contourNames)
                    
        referencedSOPInstanceUID = infoFile.ROIContourSequence.(itemNames{caseROIIndex(i,2)}). ...
            ContourSequence.(contourNames{j}).ContourImageSequence.Item_1.ReferencedSOPInstanceUID;
        dicomNamePath = [dicomDirPath,'CT.',referencedSOPInstanceUID];

        slice=double(dicomread(dicomNamePath));        
        imgInfo=dicominfo(dicomNamePath);
        
        contour=infoFile.ROIContourSequence.(itemNames{caseROIIndex(i,2)}). ...
            ContourSequence.(contourNames{j}).ContourData;
        contourX = contour(1:3:length(contour));
        contourY = contour(2:3:length(contour));
        
        [x_mm, y_mm, z_mm] = get_dicom_xyz(imgInfo);
        [x_coord_mm, y_coord_mm] = get_img_coords(x_mm, y_mm, z_mm);
        
        contourX = contourX - min(x_coord_mm);
        contourX = contourX / (abs(min(x_coord_mm)) + max(x_coord_mm)) * size(slice,1);
        
        contourY = contourY - min(y_coord_mm);
        contourY = contourY / (abs(min(y_coord_mm)) + max(y_coord_mm)) * size(slice,2);
        
        mask = double(roipoly(zeros(size(slice)),contourX,contourY));

        slices{j} = slice;
        masks{j} = mask;

    end
    
    save(dataFilePath,'slices','masks');

end
    

