% read dicom files, determine the correct ROI to use (usually GTV) and
% transofrm the selected dicom + ROI masks into .mat files

% save a dictionnary with the exact ROI (mask) name needed per patient
createDictionnary; 

% save all ROI masks names available per patient
createCaseROINames;

% save the index of the further used ROI masks per patient
createCaseROIIndex;

% save a set of ROI masks and images per patient
createDcm2Data;

% save an ROI masks and images per patient
createMatImg