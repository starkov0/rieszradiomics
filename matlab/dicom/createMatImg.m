% open the matlab set of slices + ROI masks
% choose the slice + ROI masks to extract features from and save it
% the slice is chosen as :
% 1. having 50 or more pixels in the corresponding ROI
% 2. having a maximum pixel value in the corrsponding ROI of less than 2500
% (no metal fond in the ROI)

matSETPath = '../data/matSET/';
matSETFiles = dir([matSETPath,'*.mat']);
imagePath = '../data/matIMG/';

for i=1:length(matSETFiles)
    imageName = matSETFiles(i).name;
    data = load([matSETPath,imageName]);
        
    nb = round(length(data.masks)/2);
    mask = data.masks{nb};
    slice = data.slices{nb};
    sliceMask = slice .* mask;
    ii = 1;
    ii_bin = 1;
    nbf = nb;
    
    while sum(mask(:)) < 50 || max(sliceMask(:)) > 2500
        mask = data.masks{nbf};
        slice = data.slices{nbf};
        sliceMask = slice .* mask;
        if ii_bin
            nbf = nb + ii;
        else
            nbf = nb - ii;
            ii = ii + 1;
        end
        ii_bin = abs(ii_bin-1);
    end
    
    display([
        'patientId: ', num2str(i), ...
        ' - sliceId: ',num2str(nbf), ...
        ' - number of pixels in the slice: ',num2str(sum(mask(:))), ...
        ' - maximum pixel value in the slice: ',num2str(max(sliceMask(:)))]);

    savePath=[imagePath,imageName(1:end-4),'.mat'];
    save(savePath,'slice','mask')
    
end

