% save the index of the further used ROI (mask) per patient

display('createCaseROIIndex');

dictionnary = load('../data/mat/dictionnary.mat');
dictionnary = dictionnary.dictionnary;
caseRoiNames = load('../data/mat/caseRoiNames.mat');
caseRoiNames = caseRoiNames.caseRoiNames;

caseROIIndex = [];
for i=1:size(dictionnary,1)
    if dictionnary{i,2}
        roiNames = caseRoiNames{i};
        found=0;
        for j=1:length(roiNames)     
            if strcmp(roiNames{j},dictionnary{i,2}) 
                caseROIIndex = [caseROIIndex; [i,j]];
                break;
            end
        end
    end
end

save('../data/mat/caseROIIndex.mat','caseROIIndex');
