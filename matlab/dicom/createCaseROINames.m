% save all ROI (mask) names available per patient

display('createCaseROINames');

% dicom main path
dicomPath = '../data/dicom/';
dicomDirNames = dir([dicomPath,'*00']);

caseRoiNames = {};
for i = 1:length(dicomDirNames)
    
    display(num2str(i));
    
    % dicom dir path
    dicomDirName = dicomDirNames(i).name;
    dicomDirPath = [dicomPath,dicomDirNames(i).name,'/'];

    % info file path
    infoFileStruct = dir([dicomDirPath,'RS.*']);
    infoFilePath = infoFileStruct.name;
    
    infoFile = dicominfo([dicomDirPath,infoFilePath]);
    itemNames = fieldnames(infoFile.StructureSetROISequence);
    
    roiNames={};
    for j=1:length(itemNames)
        roiNames{j} = infoFile.StructureSetROISequence.(itemNames{j}).ROIName;
    end%     
    caseRoiNames{i} = roiNames;
end

save('../data/mat/caseRoiNames.mat','caseRoiNames');

