# compute COX-LASSO survival cross-validation with survival type on inner and outer nodule parts

options(warn=-1)

# load library
library(plotrix)
library(permute)
library(Matrix)
library(foreach)
library(survivalROC)
library(glmnet)
library(survival)
library(fields)
library(graphics)
library(caTools)
library(pracma)
source("plotMatrix.R")

# load data
ll = as.matrix(read.csv('../data/csv/labels.csv'))
tumorArea = t(as.matrix(read.csv('../data/csv/tumorArea.csv')))
tumorArea = tumorArea[2,]

statusMat = matrix(nrow = 116, ncol = 6)
timeMat = matrix(nrow = 116, ncol = 6)

statusMat[,1] = (ll[,1]) > 0
statusMat[,2] = (ll[,1]+ll[,3]) > 0
statusMat[,3] = (ll[,1]+ll[,3]+ll[,5]) > 0
statusMat[,4] = (ll[,5]) > 0
statusMat[,5] = (ll[,1]+ll[,3]+ll[,5]+ll[,7]) > 0
statusMat[,6] = (ll[,7]) > 0

timeMat[,1] = apply(cbind(ll[,2]),1,min) + 1
timeMat[,2] = apply(cbind(ll[,2],ll[,4]),1,min) + 1
timeMat[,3] = apply(cbind(ll[,2],ll[,4],ll[,6]),1,min) + 1
timeMat[,4] = apply(cbind(ll[,6]),1,min) + 1
timeMat[,5] = apply(cbind(ll[,2],ll[,4],ll[,6],ll[,8]),1,min) + 1
timeMat[,6] = apply(cbind(ll[,8]),1,min) + 1

# local = Loc
# regional + local = RegLoc
# metastasis + local + regional = MetRegLoc
# metastasis = Met
# metastasis + local + regional + death = DeaMetRegLoc
# death = Dea

survivalArray = c("Loc","RegLoc","MetRegLoc","Met","DeaMetRegLoc","Dea");
rieszPassArray = c("High","Low")
gaussianArray = c('Gaussian','noGaussian');


# init
crossV = 100

for (k in 2:6) { # survial
  for (i in 1:2) { # Riesz
    for (l in 1:2) { # Gaussian
      pValMat = matrix(nrow = 5, ncol = 5);
      for (n in 1:5) {
        for (j in 1:5) {
          imgf1 = as.matrix(read.csv(paste0('../data/csv/',gaussianArray[l],'/imagingFeaturesIn',n,'_',j,'.csv')))
          imgf2 = as.matrix(read.csv(paste0('../data/csv/',gaussianArray[l],'/imagingFeaturesOut',n,'_',j,'.csv')))
          
          status = statusMat[,k]
          time = timeMat[,k]
          
          permutationsArray0 = as.matrix(read.csv(paste0('../data/csv/permutations/permutationsArray',k,0,'.csv')))
          permutationsArray1 = as.matrix(read.csv(paste0('../data/csv/permutations/permutationsArray',k,1,'.csv')))
          
          pValArray = array(dim = crossV)
          for (crossVI in 1:crossV) {
            # separate train and test indexes
            totalI0 = permutationsArray0[,crossVI]
            totalI1 = permutationsArray1[,crossVI]
            trainOutI = cbind(t(totalI0[1:(floor(size(totalI0)[2]/2))]),
                              t(totalI1[1:(floor(size(totalI1)[2]/2))]))
            testOutI = cbind(t(totalI0[(floor(size(totalI0)[2]/2)+1):size(totalI0)[2]]),
                             t(totalI1[(floor(size(totalI1)[2]/2)+1):size(totalI1)[2]]))
            
            # comute train test survival objects
            surv_obj_trainOut = Surv(time=time[trainOutI], event=status[trainOutI], type='right')
            surv_obj_testOut = Surv(time=time[testOutI], event=status[testOutI], type='right')
            cvfit = cv.glmnet(features[trainOutI,],surv_obj_trainOut,family="cox",alpha=0,nfolds=3)
            predictionsTrainOut = predict(cvfit,features[trainOutI,],s="lambda.1se")
            predictionsTestOut = predict(cvfit,features[testOutI,],s="lambda.1se")
            
            q_value = median(c(predictionsTrainOut));
            pred_upp = predictionsTestOut > q_value
            pred_low = predictionsTestOut <= q_value
            
            if (sum(pred_upp) == 0 || sum(pred_low) == 0) {
              p_value = 0.5
            } else {
              res = survdiff(surv_obj_testOut ~ pred_low)
              p_value = 1 - pchisq(res$chisq, length(res$n) - 1)
            }
            
            pValArray[crossVI] = p_value
          }
          
          pValMat[n,j] = median(pValArray);
          print(pValMat)
          
        }  
      }
      jpeg(file=paste0("../data/img/pvalue_innerOuter_",survivalArray[k],"_",rieszPassArray[i],"_",gaussianArray[l],".jpeg"))
      plotMatrix(pValMat,5,
                 "Riesz J parameter",
                 "Riesz N parameter",
                 paste0("P-values - ",survivalArray[k]," - ",gaussianArray[l]," - ",rieszPassArray[i]," Pass Riesz"))
      dev.off()
    }
  }
}
